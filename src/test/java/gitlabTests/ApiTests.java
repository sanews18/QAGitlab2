package gitlabTests;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
@Tag("API")
public class ApiTests {

    String token = "66DF165248E944057DC26713487C65842089AFB9538ABDCDF903960E9908E72E20785B61AD4C641A49FD6C6A194E25BEF1E7D6394EF02FC644CF1475432F844FE15A16F1DEF78FCD49420CBD395F60EF99A74E29640907B55AC8509ED4F7392DA18B119CE85A48B64277F7C9E45B2862E849D831062DCB725490363E21A090B896AE5187D64015017C25B442F6B0029FDF5023C34A169C39935AF054BC5E94719295925FCE1716797F3384FD7FB1B22AF8758AF4F6F591C49BC7586B9FC822C40CE39C62586E122E2B086A3BC55053223358C845298CFBE431A1E36AE4C687420B488CB0D71F7A6068E44D6F9F4995231CE12FB6BDCAB6CAF35C2055163C8D48848E4C341E16869ABB2ECB6B497DC7CA735E72689D1BB6ECBD3541E45D0FAFC7F2C17B7B38EA1AAADF23CFDDA00AA98BD08715F6027AA6C5528E550AAC02DB9747825683A63C8BDAA22FE22E031D952521D2C1084490543F7C7FD41C5157E459FCE1C960BBAEA0C31DCDB0ED7B887A11ACD05312E40A6A2E6523C97D6D5C85FCF5E2486449BE9F264C1CAD71ADD210F7B4BE7977";
    String idDocument = "2011562";

    String Doc = "<DocumentCard>\n" +
            "  <Comment>64757575756</Comment>\n" +
            "  <Number>1</Number>\n" +
            "  <Date>2022-02-24T14:34:40.9632729+04:00</Date>\n" +
            "  <RequestSign>true</RequestSign>\n" +
            "  <Contract>\n" +
            "    <Number>545454</Number>\n" +
            "    <Date>2022-08-17</Date>\n" +
            "  </Contract>  \n" +
            "  <NetSum>1</NetSum>\n" +
            "  <VatSum>0.20</VatSum>\n" +
            "  <TotalSum>1.20</TotalSum>\n" +
            "  <TypeCode>UPD_SVZK</TypeCode>\n" +
            "  <Barcode>\n" +
            "    <Type>Code128</Type>\n" +
            "    <Value>barcodtest</Value>\n" +
            "  </Barcode>\n" +
            "  <Content>\n" +
            "    <Filename>ON_NSCHFDOPPR_2BK-7604217626-4085_2BK-780107457248-7841_20220224_32A46C05-1429-45B6-8BCD-D22028693578.xml</Filename>\n" +
            "    <MimeType>text/xml</MimeType>\n" +
            " <Content>PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0id2luZG93cy0xMjUxIj8+CjzU4OnrIHhtbG5zOnhzZD0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEiIHhtbG5zOnhzaT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEtaW5zdGFuY2UiIMjk1ODp6z0iT05fTlNDSEZET1BQUl8yQkstNzYwNDIxNzYyNi00MDg1XzJCSy03ODAxMDc0NTcyNDgtNzg0MV8yMDIyMDIyNF8zMkE0NkMwNS0xNDI5LTQ1QjYtOEJDRC1EMjIwMjg2OTM1NzgiIMLl8PHP8O7jPSLd6+Xq8vDu7e376SDK8/D85fAgMS4wIiDC5fDx1O7w7D0iNS4wMSI+CiAgPNHi0/fE7urO4e7wIMjkzvLv8D0iMkJLLTc4MDEwNzQ1NzI0OC03ODQxIiDI5M/u6z0iMkJLLTc2MDQyMTc2MjYtNDA4NSI+CiAgICA80eLO3cTO8u/wIM3g6OzO8OM9Is7OziDKztDT0SDK7u3x4Ovy6O3jINHNwyAoMkJLKSIgyM3N3ss9Ijc4MDEzOTIyNzEiIMjk3cTOPSIyQksiIC8+CiAgPC/R4tP3xO7qzuHu8D4KICA8xO7q8+zl7fIgys3EPSIxMTE1MTMxIiDP7tTg6vLVxj0ixO7q8+zl7fIsIOLq6/734P756Okg4iDx5eH/IPHi5eTl7ej/IO4g9ODq8uUg5+Dq8+/q6CDq7uzo8fHo7u3l8O7sICjg4+Xt8u7sLCDk5enx8uLz/vno7CDu8iDx7uHx8uLl7e3u4+4g6Ozl7egpIPLu4uDw7uIg5Ov/IOru7Ojy5e3y4CAo7/Do7fbo7+Dr4CkiIM3g6OzE7urO7/A9IsTu6vPs5e3yLCDi6uv+9+D++ejpIOIg8eXh/yDx4uXk5e3o/yDuIPTg6vLlIOfg6vPv6ugg6u7s6PHx6O7t5fDu7CAo4OPl7fLu7Cwg5OXp8fLi8/756Owg7vIg8e7h8fLi5e3t7uPuIOjs5e3oKSDy7uLg8O7iIOTr/yDq7uzo8uXt8uAgKO/w6O326O/g6+ApIiDE4PLgyO30z/A9IjI0LjAyLjIwMjIiIMLw5ezI7fTP8D0iMTUuMjAuMzAiIM3g6Ozd6u7t0fPh0e7x8j0izsHZxdHSws4g0SDOw9DAzcjXxc3NzskgztLCxdLR0sLFzc3O0dLc3iAmcXVvdDvO18DDJnF1b3Q7IDc3NTE1MjE4NTAvNzc1MTAxMDAxIiDU8+3q9uj/PSLR4sfKIj4KICAgIDzR4tH31ODq8iDN7uzl8NH31D0i9unz6SIgxODy4NH31D0iMjQuMDIuMjAyMiIgyu7kzsrCPSI2NDMiPgogICAgICA80eLP8O7kPgogICAgICAgIDzI5NHiPgogICAgICAgICAgPNHi3svT9yDN4OjszvDjPSLOzs4gJnF1b3Q7ztfAwyZxdW90OyIgyM3N3ss9Ijc3NTE1MjE4NTAiIMrPzz0iNzc1MTAxMDAxIiAvPgogICAgICAgIDwvyOTR4j4KICAgICAgICA8wOTw5fE+CiAgICAgICAgICA8wOTw0NQgyO3k5erxPSIxNDI3ODQiIMru5NDl4+ju7T0iNTAiIMPu8O7kPSLjIMzu8eru4vHq6OkiINPr6PbgPSLs6vAgMS3pIiDE7uw9IjI1IiDK4uDw8j0iNTQiIC8+CiAgICAgICAgPC/A5PDl8T4KICAgICAgICA8weDt6tDl6uIgze7s5fDR9+Xy4D0iNDA3MDI4MTA2MjIxMjAwMDA2NTciPgogICAgICAgICAgPNHiweDt6iDN4OjsweDt6j0i1MjLyMDLIM/AziAmcXVvdDvBwM3KINPQwMvRyMEmcXVvdDsgwiDDLtHAzcrSLc/F0sXQwdPQwyIgwcjKPSIwNDQwMzA3MDYiIMru8NH35fI9IjMwMTAxODEwODAwMDAwMDAwNzA2IiAvPgogICAgICAgIDwvweDt6tDl6uI+CiAgICAgIDwv0eLP8O7kPgogICAgICA8w/Dz587yPgogICAgICAgIDzO7cblPu7tIOblPC/O7cblPgogICAgICA8L8Pw8+fO8j4KICAgICAgPNHiz+7q8+8+CiAgICAgICAgPMjk0eI+CiAgICAgICAgICA80eLey9P3IM3g6OzO8OM9Is7OziAmcXVvdDvQwNPVyMwmcXVvdDsiIMjNzd7LPSI1MjQ5MTM3MjEwIiDKz889IjUyNDkwMTAwMSIgLz4KICAgICAgICA8L8jk0eI+CiAgICAgICAgPMDk8OXxPgogICAgICAgICAgPMDk8NDUIMjt5OXq8T0iNjA2MDI1IiDK7uTQ5ePo7u09IjUyIiDD7vDu5D0i4yDE5+Xw5ujt8eoiINPr6PbgPSLv8C3q8iDW6O7r6u7i8eru4+4iIMTu7D0iMjQzIiDK4uDw8j0iMTUiIC8+CiAgICAgICAgPC/A5PDl8T4KICAgICAgICA8weDt6tDl6uIgze7s5fDR9+Xy4D0iNDQ0MzQ4MTA0MjQ4OTc1Njk4NTAiPgogICAgICAgICAgPNHiweDt6iDN4OjsweDt6j0iz8DOINHBxdDBwM3KIiDByMo9IjA0NDUyNTIyNSIgyu7w0ffl8j0iMzAxMDE4MTA0MDAwMDAwMDAyMjUiIC8+CiAgICAgICAgPC/B4O3q0OXq4j4KICAgICAgPC/R4s/u6vPvPgogICAgICA8xO7v0eLU1cYxIM3g6OzOysI9ItDu8fHo6fHq6Okg8PPh6/wiPgogICAgICAgIDzO8e3T8fLE5e3S8OXhIM3g6OzO8e09IsHl5yDk7urz7OXt8uAt7vHt7uLg7ej/IiAvPgogICAgICA8L8Tu79Hi1NXGMT4KICAgICAgPMTu6s/u5PLizvLj8CDN4OjsxO7qzvLj8D0iMjM0MjM0MiIgze7sxO7qzvLj8D0iMTIzMTIzIiDE4PLgxO7qzvLj8D0iMjQuMDIuMjAyMiIgLz4KICAgICAgPMjt9M/u69TVxjEgLz4KICAgIDwv0eLR99Tg6vI+CiAgICA80uDh69H31ODq8j4KICAgICAgPNHi5eTS7uIgze7s0fLwPSIxIiDN4Ojs0u7iPSLx9+z/9/HsIiDOysXIX9Lu4j0iMTE1IiDK7uvS7uI9IjM1NDM1MzQ1IiDN4OvR8j0iMjAlIiDR8tLu4tP3zeDrPSIwIj4KICAgICAgICA8wOr26Oc+CiAgICAgICAgICA8weXnwOr26Oc+4eXnIODq9ujn4DwvweXnwOr26Oc+CiAgICAgICAgPC/A6vbo5z4KICAgICAgICA80fPszeDrPgogICAgICAgICAgPMTl9M3E0T4tPC/E5fTNxNE+CiAgICAgICAgPC/R8+zN4Os+CiAgICAgICAgPMTu79Hi5eTS7uIgz/DS7uLQ4OE9IjUiIM3g6OzF5Mjn7D0iMTBeOSDsMyIgLz4KICAgICAgPC/R4uXk0u7iPgogICAgICA8wvHl4+7O7+sg0fLS7uLB5efNxNHC8eXj7j0iMCIg0fLS7uLT983g68Lx5ePuPSIwIj4KICAgICAgICA80fPszeDrwvHl4+4+CiAgICAgICAgICA80fPszeDrPjA8L9Hz7M3g6z4KICAgICAgICA8L9Hz7M3g68Lx5ePuPgogICAgICA8L8Lx5ePuzu/rPgogICAgPC/S4OHr0ffU4OryPgogICAgPNHiz/Du5M/l8D4KICAgICAgPNHiz+XwINHu5M7v5fA9IvPq9vPq9vMiPgogICAgICAgIDzO8e3P5fAgzeDo7M7x7T0iweXnIOTu6vPs5e3y4C3u8e3u4uDt6P8iIC8+CiAgICAgICAgPNLw4O3D8PPnINHi0vDg7cPw8+c9IvPq9vPq9vPqIiAvPgogICAgICAgIDzR4s/l8MLl+eggLz4KICAgICAgPC/R4s/l8D4KICAgICAgPMjt9M/u69TVxjMgLz4KICAgIDwv0eLP8O7kz+XwPgogICAgPM/u5O/o8eDt8iDO4evP7uvtPSIwIiDR8uDy8/E9IjEiIM7x7c/u6+09IsTu6+bt7vHy7fvlIO7h/+fg7e3u8fLoIiDO8e3P7uvtzvDjPSLE7uLl8OXt7e7x8vwg0M8iPgogICAgICA83ssgyM3N3ss9Ijc3NTE1MjE4NTAiIM3g6OzO8OM9Is7OziAmcXVvdDvO18DDJnF1b3Q7IiDE7uvm7T0iw+Xt5fDg6/zt++kg5Ojw5ery7vAiIMjt++XR4uXkPSLP8O7i5fDq4CDk7uLl8OXt7e7x8uggz+7k7+jx4O3y4CI+PNTIziDU4Ozo6+j/PSLB4Obg7e7iIiDI7P89ItHl8OPl6SIgzvL35fHy4u49IsDr5erx4O3k8O7i6PciIC8+PC/eyz4KICAgIDwvz+7k7+jx4O3yPgogIDwvxO7q8+zl7fI+Cjwv1ODp6z4=\n" +
            "    </Content>\n" +
            "    </Content>\n" +
            "</DocumentCard>";
    @BeforeAll
    public static void setBaseUrl(){
        RestAssured.baseURI = "http://courier-demo.esphere.ru/api/api/";
    }

         @Test
    public void getPDF(){
        given().header("content-type", "application/json")
                .header("Auth-Token", token)
                .get("/document/pdf/" + idDocument)
                .then().log().body().statusCode(200)
                .body("mimeType", equalTo("application/pdf"));
    }

    @Test
    public void getPDFerror(){
        given().header("content-type", "application/json")
                .header("Auth-Token", token)
                .get("/document/pdf/" + idDocument + "1")
                .then().log().body().statusCode(400)
                .body("message", equalTo("ER-20006: Ошибка входных параметров."));
    }

    @Test
    public void createDocument(){
        given().header("Content-Type", "application/xml")
                .header("Auth-Token", token)
                .header("Connection", "keep-alive")
                .body(Doc)
                .post("document/add/2BK-7604217626-4085")
                .then().log().body().statusCode(200);
                //        .body("Document", equalTo("Id"));
    }

    @Test
    public void createDocumentError(){
        given().header("Content-Type", "application/xml")
                .header("Auth-Token", token)
                .header("Connection", "keep-alive")
                .body(Doc)
                .post("document/add/2BK-7604217626-40852")
                .then().log().body().statusCode(400);
        //        .body("Document", equalTo("Id"));
    }


//    @BeforeAll
//    public static void setBaseUrl(){
//        RestAssured.baseURI = "https://api.spacexdata.com/v4";
//    }
//
//    @Test
//    public void checkCeoIsElonMusk(){
//        given().get("/company")
//                .then().log().body()
//                .body("ceo", equalTo("Elon Musk"));
//    }
//
//    @Test
//    public void checkLinksIsFour(){
//        LinkedHashMap<String, String> links = given().get("/company")
//                .then().log().body()
//                .extract().body().jsonPath().get("links");
//        Assertions.assertEquals(4, links.size());
//    }
//
//    @Test
//    public void crewMembersIs30(){
//        ArrayList members = given().get("/crew")
//                .then().log().body()
//                .extract().body().as(ArrayList.class);
//        Assertions.assertTrue(members.size() == 30);
//    }
//
//    @Test
//    public void firstMemberIsRobert(){
//        ArrayList members = given().get("/crew")
//                .then()
//                .extract().body().as(ArrayList.class);
//
//        LinkedHashMap firstMember = (LinkedHashMap) members.get(0);
//        String id = (String) firstMember.get("id");
//
//        Response response = given().get("/crew/"+id)
//                .then().log().body()
//                .extract().response();
//
//        JsonPath jsonPath = response.jsonPath();
//
//        String name = jsonPath.getString("name");
//
//        Assertions.assertEquals("Robert Behnken", name);
//    }
//
//    @Test
//    public void lastMemberHasNoLaunches(){
//        List<MemberPojo> members = given().get("/crew")
//                .then()
//                .extract().body().jsonPath().getList("",MemberPojo.class);
//
//        String lastMemberId = members.get(members.size()-1).getId();
//
//        MemberPojo lastMember = given().get("/crew/"+lastMemberId)
//                .then().log().body()
//                .extract().as(MemberPojo.class);
//
//        Assertions.assertTrue(lastMember.getLaunches().size()==0);
//    }

}
